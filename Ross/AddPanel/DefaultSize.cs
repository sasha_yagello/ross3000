﻿namespace Ross.AddPanel
{
    public class DefaultSize
    {
        public const double sizeChat = 200;

        public const double sizeSetting = 200;

        public const double sizeTopTable = 200;

        public const double sizeLeftDownTable = 90;

        public const double sizeDownTable = -1;
    }
}